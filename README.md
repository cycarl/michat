# michat 即时通讯app。

#### 介绍
一个即时通讯app, 用户可以发送文字、emoji和图片，语音消息，语音和视频聊天功能测试ing。

#### 软件架构
采用标准MVP模式, 模块化开发。

#### 截图

<center class="half">
<img src="https://mikochat.oss-ap-northeast-1.aliyuncs.com/Screenshoots/mikochat/0.jpg" width="200px"/>
<img src="https://mikochat.oss-ap-northeast-1.aliyuncs.com/Screenshoots/mikochat/1.jpg" width="200px"/>
<img src="https://mikochat.oss-ap-northeast-1.aliyuncs.com/Screenshoots/mikochat/2.jpg" width="200px"/>
</center>
<center class="half">
<img src="https://mikochat.oss-ap-northeast-1.aliyuncs.com/Screenshoots/mikochat/3.jpg" width="200px"/>
<img src="https://mikochat.oss-ap-northeast-1.aliyuncs.com/Screenshoots/mikochat/4.jpg" width="200px"/>
<img src="https://mikochat.oss-ap-northeast-1.aliyuncs.com/Screenshoots/mikochat/5.jpg" width="200px"/>
</center>
<center class="half">
<img src="https://mikochat.oss-ap-northeast-1.aliyuncs.com/Screenshoots/mikochat/6.jpg" width="200px"/>
<img src="https://mikochat.oss-ap-northeast-1.aliyuncs.com/Screenshoots/mikochat/7.jpg" width="200px"/>
<img src="https://mikochat.oss-ap-northeast-1.aliyuncs.com/Screenshoots/mikochat/8.jpg" width="200px"/>
</center>

<center class="half">
<img src="https://mikochat.oss-ap-northeast-1.aliyuncs.com/Screenshoots/mikochat/9.jpg" width="200px"/>
<img src="https://mikochat.oss-ap-northeast-1.aliyuncs.com/Screenshoots/mikochat/10.jpg" width="200px"/>
<img src="https://mikochat.oss-ap-northeast-1.aliyuncs.com/Screenshoots/mikochat/12.jpg" width="200px"/>
</center>


#### 参与贡献
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
